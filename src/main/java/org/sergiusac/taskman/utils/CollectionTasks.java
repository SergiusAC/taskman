package org.sergiusac.taskman.utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.sergiusac.taskman.objects.Task;

/**
 * Created by Sergey Cheen on 7/2/17.
 */
public class CollectionTasks {
    ObservableList<Task> tasks = FXCollections.observableArrayList();

    public void add(Task task) {
        tasks.add(task);
    }

    public void update(Task task) {
    }

    public void delete(Task task) {
        tasks.remove(task);
    }

    public ObservableList<Task> getList() {
        return tasks;
    }
}
