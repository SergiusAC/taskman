package org.sergiusac.taskman.utils.dbservice;

import org.sergiusac.taskman.objects.Task;
import org.sergiusac.taskman.utils.dbservice.dao.TasksDAO;
import org.sergiusac.taskman.utils.dbservice.datasets.TasksDataSet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Sergey Cheen on 7/9/17.
 */
public class DBService {
    private final Connection connection;

    public DBService() {
        if ((this.connection = getSQLiteConnection()) == null) {
            System.err.println("SQLite connection error");
        } else {
            System.out.println("Connection to SQLite has been established");
        }
    }

    public TasksDataSet getTask(long id) {
        TasksDataSet tasksDataSet = null;
        try {
            tasksDataSet = new TasksDAO(connection).get(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tasksDataSet;
    }

    public long getTaskId(Task task) {
        long id = 0;
        try {
            TasksDAO dao = new TasksDAO(connection);
            id = dao.getTaskId(task.getTaskTitle(), task.getTaskText(), task.getTaskDate());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public long addTask(Task task) {
        long id = 0;
        try {
            connection.setAutoCommit(false);
            TasksDAO dao = new TasksDAO(connection);
            dao.insertTask(task);
            connection.setAutoCommit(true);
            id = dao.getTaskId(task.getTaskTitle(), task.getTaskText(), task.getTaskDate());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            return id;
        }
    }

    public void updateTask(Task task, long id) {
        try {
            new TasksDAO(connection).updateTask(task, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getSQLiteConnection() {
        Connection conn = null;
        try {
            Class.forName("org.sqlite.JDBC");
            StringBuilder url = new StringBuilder();
            url.
                    append("jdbc:").
                    append("sqlite:").
                    append("tasks.db");
            conn = DriverManager.getConnection(url.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (conn != null) {
            return conn;
        } else {
            return null;
        }
    }
}
