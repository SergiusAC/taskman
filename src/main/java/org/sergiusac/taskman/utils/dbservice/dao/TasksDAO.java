package org.sergiusac.taskman.utils.dbservice.dao;

import org.sergiusac.taskman.objects.Task;
import org.sergiusac.taskman.utils.dbservice.datasets.TasksDataSet;
import org.sergiusac.taskman.utils.dbservice.executor.Executor;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * Created by Sergey Cheen on 7/9/17.
 */
public class TasksDAO {
    private Executor executor;

    public TasksDAO(Connection connection) {
        this.executor = new Executor(connection);
    }

    public TasksDataSet get(long id) throws SQLException {
        return executor.execQuery("SELECT * FROM tasks WHERE id = " + id, result -> {
            result.next();
            return new TasksDataSet(result.getLong(1), result.getString(2),
                    result.getString(3), LocalDate.parse(result.getString(4)));
        });
    }

    public long getTaskId(String title, String content, LocalDate date) throws SQLException {
        return executor.execQuery(
                "SELECT * FROM tasks WHERE title = '" + title +
                "' and content = '" + content + "' and date = '" + date.toString() + "'", result -> {
                    result.next();
                    return result.getLong(1);
                });
    }

    public void insertTask(Task task) throws SQLException {
        executor.execUpdate("INSERT INTO tasks (title, content, date) " +
                            "VALUES ('" + task.getTaskTitle() + "', '" + task.getTaskText() + "', '" + task
                .getTaskDate() + "')");
    }

    public void updateTask(Task task, long id) throws SQLException {
        executor.execUpdate("UPDATE tasks SET title = '" + task.getTaskTitle() + "', content = '" + task
                .getTaskText() + "', date = '" + LocalDate.now() + "' WHERE id = " + id);
    }
}
