package org.sergiusac.taskman.utils.dbservice.datasets;

import java.time.LocalDate;

/**
 * Created by Sergey Cheen on 7/9/17.
 */
public class TasksDataSet {
    private long id;
    private String title;
    private String content;
    private LocalDate date;

    public TasksDataSet(long id, String title, String content, LocalDate date) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "TasksDataSet{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", date=" + date +
                '}';
    }
}
