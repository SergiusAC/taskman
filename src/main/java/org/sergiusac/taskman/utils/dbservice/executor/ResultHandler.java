package org.sergiusac.taskman.utils.dbservice.executor;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Sergey Cheen on 7/9/17.
 */
public interface ResultHandler<T> {
    T handle(ResultSet set) throws SQLException;
}
