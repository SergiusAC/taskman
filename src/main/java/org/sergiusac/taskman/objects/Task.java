package org.sergiusac.taskman.objects;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDate;

/**
 * Created by Sergey Cheen on 7/2/17.
 */
public class Task {
    private StringProperty taskTitle;
    private StringProperty taskText;
    private ObjectProperty<LocalDate> taskDate;

    public Task() {
        this(null, null, null);
    }

    public Task(String title, String text , LocalDate date) {
        taskTitle = new SimpleStringProperty(title);
        taskText = new SimpleStringProperty(text);
        taskDate = new SimpleObjectProperty<LocalDate>(date);
    }

    public String getTaskTitle() {
        return taskTitle.get();
    }

    public void setTaskTitle(String title) {
        taskTitle.set(title);
    }

    public StringProperty taskTitleProperty() {
        return taskTitle;
    }

    public String getTaskText() {
        return taskText.get();
    }

    public void setTaskText(String text) {
        taskText.set(text);
    }

    public StringProperty taskTextProperty() {
        return taskText;
    }

    public LocalDate getTaskDate() {
        return taskDate.get();
    }

    public void setTaskDate(LocalDate date) {
        taskDate.set(date);
    }

    public ObjectProperty<LocalDate> taskDateProperty() {
        return taskDate;
    }

    public String getTaskDateString() {
        return  taskDate.get().toString();
    }
}
