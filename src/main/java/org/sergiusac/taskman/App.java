package org.sergiusac.taskman;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.sergiusac.taskman.objects.Task;
import org.sergiusac.taskman.utils.CollectionTasks;

import java.io.IOException;
import java.time.LocalDate;

/**
 * Created by Sergey Cheen on 7/1/17.
 */
public class App extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Taskman");

        initRootLayout();
        showMainWindow();
    }

    private void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/RootLayout.fxml"));
            rootLayout = loader.load();
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.setMinHeight(500);
            primaryStage.setMinWidth(400);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showMainWindow() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/TasksOverview.fxml"));
            AnchorPane tasksOverview = loader.load();
            rootLayout.setCenter(tasksOverview);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static CollectionTasks initObservableList() {
        CollectionTasks tasks = new CollectionTasks();
        tasks.add(new Task("Task 1", "xtxttx", LocalDate.of(2017, 12, 12)));
        tasks.add(new Task("Task 2", "xtxttx", LocalDate.of(2017, 2, 15)));
        tasks.add(new Task("Task 3", "xtxttx", LocalDate.of(2016, 6, 16)));
        tasks.add(new Task("Task 4", "xtxttx", LocalDate.of(2015, 7, 8)));
        tasks.add(new Task("Task 5", "xtxttx", LocalDate.of(2015, 8, 26)));
        tasks.add(new Task("Task 6", "xtxttx", LocalDate.of(2014, 10, 30)));
        return  tasks;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
