package org.sergiusac.taskman.controllers;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.sergiusac.taskman.objects.Task;
import org.sergiusac.taskman.utils.CollectionTasks;

import java.time.LocalDate;

/**
 * Created by Sergey Cheen on 7/3/17.
 */
public class EditTaskController {

    @FXML
    public TextField txtTitle;

    @FXML
    public TextArea txtContent;

    @FXML
    public Button btnOk;

    @FXML
    public Button btnCancel;

    private CollectionTasks tasks;
    private Task editingTask = null;

    @FXML
    public void initialize() {
    }

    public void setTasks(CollectionTasks tasks) {
        this.tasks = tasks;
    }

    public void setTxtTitle(String title) {
        txtTitle.setText(title);
    }

    public void setTxtContent(String content) {
        txtContent.setText(content);
    }

    public void setEditingTask(Task task) {
        editingTask = task;
    }

    public void actionButtonPressed(ActionEvent actionEvent) {
        Node node = (Node) actionEvent.getSource();
        switch (node.getId()) {
            case "btnOk":
                btnOkPressed();
                break;
            case "btnCancel":
                btnCancelPressed(node);
                break;
        }
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
    }

    private void btnOkPressed() {
        ObservableList<Task> taskList = tasks.getList();
        if (editingTask != null) {
            Task task = taskList.get(taskList.indexOf(editingTask));
            task.setTaskTitle(txtTitle.getText());
            task.setTaskText(txtContent.getText());
            task.setTaskDate(LocalDate.now());
        } else {
            taskList.add(new Task(txtTitle.getText(), txtContent.getText(), LocalDate.now()));
        }
    }

    private void btnCancelPressed(Node node) {
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
    }
}
