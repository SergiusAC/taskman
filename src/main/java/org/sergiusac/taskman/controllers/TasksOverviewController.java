package org.sergiusac.taskman.controllers;

import javafx.collections.ListChangeListener;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.sergiusac.taskman.App;
import org.sergiusac.taskman.objects.Task;
import org.sergiusac.taskman.utils.CollectionTasks;

import java.io.IOException;

/**
 * Created by Sergey Cheen on 7/2/17.
 */
public class TasksOverviewController {

    @FXML
    public TextField textFieldSearch;

    @FXML
    public Button btnAdd;

    @FXML
    public Button btnEdit;

    @FXML
    public Button btnDelete;

    @FXML
    public TableView<Task> tableViewTask;

    @FXML
    public TableColumn<Task, String> tableColumnTask;

    @FXML
    public TableColumn<Task, String> tableColumnDate;

    @FXML
    public Label labelCount;

    private CollectionTasks tasks;
    private FXMLLoader loader;
    private Parent editTask;
    private Stage editTaskStage;
    private EditTaskController editTaskController;

    public TasksOverviewController() {
        //tasks = new CollectionTasks();
        tasks = App.initObservableList();
    }

    @FXML
    public void initialize() {
        tableColumnTask.setCellValueFactory(new PropertyValueFactory<>("taskTitle"));
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<>("taskDate"));
        initListeners();
        initLoader();
    }

    private void initLoader() {
        loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/EditTask.fxml"));
        try {
            editTask = loader.load();
            editTaskController = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initListeners() {
        tasks.getList().addListener((ListChangeListener<Task>) c -> updateCountLabel());

        tableViewTask.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2) {
                btnEditPressed((Node) event.getSource(), tableViewTask.getSelectionModel().getSelectedItem());
            }
        });

        FilteredList<Task> filteredList = new FilteredList<>(tasks.getList(), p -> true);
        textFieldSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredList.setPredicate(task -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();
                if (task.getTaskTitle().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (task.getTaskDateString().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });
        SortedList<Task> sortedList = new SortedList<>(filteredList);
        sortedList.comparatorProperty().bind(tableViewTask.comparatorProperty());
        tableViewTask.setItems(sortedList);
    }

    private void updateCountLabel() {
        labelCount.setText("Количество записей: " + tasks.getList().size());
    }

    public void actionButtonPressed(ActionEvent actionEvent) {
        Node btnPressed = (Node) actionEvent.getSource();
        switch (btnPressed.getId()) {
            case "btnAdd":
                btnAddPressed(btnPressed);
                break;
            case "btnEdit":
                btnEditPressed(btnPressed, tableViewTask.getSelectionModel().getSelectedItem());
                break;
            case "btnDelete":
                btnDeletePressed();
                break;
        }
    }

    private void btnAddPressed(Node node) {
        try {
            editTaskController.setTxtTitle("");
            editTaskController.setTxtContent("");
            editTaskController.setTasks(tasks);
            editTaskController.setEditingTask(null);
            showEditTask(node, "Добавление записи");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void btnEditPressed(Node node, Task task) {
        try {
            editTaskController.setTxtTitle(task.getTaskTitle());
            editTaskController.setTxtContent(task.getTaskText());
            editTaskController.setTasks(tasks);
            editTaskController.setEditingTask(task);
            showEditTask(node, "Изменение записи");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void btnDeletePressed() {
        Task task = tableViewTask.getSelectionModel().getSelectedItem();
        tasks.delete(task);
    }

    private void showEditTask(Node node, String title) throws IOException {
        if (editTaskStage == null) {
            editTaskStage = new Stage();
            editTaskStage.setScene(new Scene(editTask));
            editTaskStage.setMinWidth(300);
            editTaskStage.setMinHeight(300);
            editTaskStage.setTitle(title);
            editTaskStage.initModality(Modality.WINDOW_MODAL);
            editTaskStage.initOwner(node.getParent().getScene().getWindow());
        }
        editTaskStage.showAndWait();
    }
}
